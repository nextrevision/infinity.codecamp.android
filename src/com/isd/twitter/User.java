package com.isd.twitter;

import java.util.ArrayList;
import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class User extends Activity {
	
	private Typeface mTypeface = null;
	private Activity mActivity;
	private ProgressDialog mDialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.user);

		setProgressBarIndeterminateVisibility(true);

		mActivity = this;
		
		mDialog = new ProgressDialog(mActivity);
        mDialog.setIndeterminate(true);
		mDialog.setCancelable(false);
		mDialog.setTitle("Please wait");
		mDialog.setMessage("Retrieving profile...");
		mDialog.show();

		mTypeface = Typeface.createFromAsset(getApplicationContext().getAssets(),
				"fonts/Mako-Regular.ttf");

		Bundle extras = getIntent().getExtras();
		String userId = extras.getString("id");
		
		AsyncHelper asyncHelper = new AsyncHelper(this.getLocalClassName());
		asyncHelper.setUser(this);
		asyncHelper.execute(userId);
		
		
	}

	public void onComplete(DTO dto) {
		
		ListView mListView = (ListView)findViewById(android.R.id.list);
		
		ImageView userPicture = (ImageView) findViewById(R.id.userPicture);
		ImageView userLock = (ImageView) findViewById(R.id.userLock);
		TextView userName = (TextView) findViewById(R.id.userName);
		TextView userHandle = (TextView) findViewById(R.id.userHandle);
		TextView userDescription = (TextView) findViewById(R.id.userDescription);
		TextView userTweets = (TextView) findViewById(R.id.userTweets);
		TextView userFollowing = (TextView) findViewById(R.id.userFollowing);
		TextView userFollowers = (TextView) findViewById(R.id.userFollowers);
		TextView userFavorites = (TextView) findViewById(R.id.userFavorites);

		userPicture.setImageBitmap(dto.bitmap);
		userName.setText(dto.user.name);
		userName.setTypeface(mTypeface);
		userHandle.setText(dto.user.handle);
		userDescription.setText(dto.user.description);
		userTweets.setText(Long.toString(dto.user.statuses));
		userFollowing.setText(Long.toString(dto.user.friends));
		userFollowers.setText(Long.toString(dto.user.followers));
		userFavorites.setText(Long.toString(dto.user.favorites));
		
		// User profile visibility defaults to false
		// 		Don't really need to worry since their
		//		tweet was public anyways, but crazier
		//		things have happened.
		if (!dto.user.profileProtected) {
			ArrayList<TweetModel> tweets = dto.tweets;
			UserCustomAdapter mAdapter = new UserCustomAdapter(User.this, R.layout.user_list_item, tweets);
			userLock.setVisibility(View.INVISIBLE);
	        mListView.setAdapter(mAdapter);
		} else {
			userLock.setVisibility(View.VISIBLE);
		}
		
		setProgressBarIndeterminateVisibility(false);
		
		mDialog.hide();

	}

}
