package com.isd.twitter;

import java.text.SimpleDateFormat;
import java.util.Date;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

public class Tweet extends Activity {
	private Typeface mTypeface = null;
	private Activity mActivity;
	private ProgressDialog mDialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tweet);

		setProgressBarIndeterminateVisibility(true);

		mActivity = this;
		
		mDialog = new ProgressDialog(mActivity);
        mDialog.setIndeterminate(true);
		mDialog.setCancelable(false);
		mDialog.setTitle("Please wait");
		mDialog.setMessage("Retrieving tweet...");
		mDialog.show();

		mTypeface = Typeface.createFromAsset(getApplicationContext().getAssets(),
				"fonts/Mako-Regular.ttf");

		Bundle extras = getIntent().getExtras();
		String tweetId = extras.getString("id");
		AsyncHelper asyncHelper = new AsyncHelper(this.getLocalClassName());
		asyncHelper.setTweet(this);
		asyncHelper.execute(tweetId);
		
	}

	public void onResourceComplete(final DTO dto) {
		
		ImageView tweetPicture = (ImageView) findViewById(R.id.tweetPicture);
		TextView tweetName = (TextView) findViewById(R.id.tweetName);
		TextView tweetHandle = (TextView) findViewById(R.id.tweetHandle);
		TextView tweetDate = (TextView) findViewById(R.id.tweetDate);
		TextView tweetText = (TextView) findViewById(R.id.tweetText);
		TextView tweetRT = (TextView) findViewById(R.id.tweetRT);

		Date date = new Date(Date.parse(dto.tweetModel.createdDate));
		SimpleDateFormat sDate = new SimpleDateFormat(
				"EEE, MMM dd, yyyy 'at' h:mm a");
		String newDate = sDate.format(date);

		tweetPicture.setImageBitmap(dto.bitmap);
		tweetName.setText(dto.tweetModel.user.name);
		tweetName.setTypeface(mTypeface);
		tweetHandle.setText(dto.tweetModel.user.handle);
		tweetDate.setText(newDate);
		tweetText.setText(dto.tweetModel.text);
		tweetText.setTypeface(mTypeface);
		tweetRT.setText(dto.tweetModel.rtCount == 0 ? "Retweets: " + Integer.toString(dto.tweetModel.rtCount) : "No retweets");

		setProgressBarIndeterminateVisibility(false);
		
		mDialog.hide();
		
		tweetName.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent in = new Intent(mActivity, User.class);
				in.putExtra("id", dto.tweetModel.user.idStr);
				startActivity(in);
			}
			
		});

	}
}
