package com.isd.twitter;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CustomAdapter extends ArrayAdapter<TweetModel> {
	private ArrayList<TweetModel> tweets;
	private Activity activity;
	private TextView tweetContent;
	private TextView tweetAuthor;
	private Typeface tf;

	public CustomAdapter(Activity activity, int viewId,
			ArrayList<TweetModel> tweets) {
		super(activity, R.layout.list_item, tweets);
		this.activity = activity;
		this.tweets = tweets;
		tf = Typeface.createFromAsset(activity.getAssets(),
				"fonts/Mako-Regular.ttf");
	}

	public View getView(int position, View v, ViewGroup parent) {
		View view = v;
		TweetModel tweet = tweets.get(position);
		if (v == null) {
			LayoutInflater inflator = activity.getLayoutInflater();
			view = inflator.inflate(R.layout.list_item, null);
			tweetContent = (TextView) view.findViewById(R.id.searchTweet);
			tweetAuthor = (TextView) view.findViewById(R.id.searchUser);
		}
		if (tweet != null) {
			tweetAuthor.setText(tweet.fromUser);
			tweetContent.setText(tweet.text);
			tweetContent.setTypeface(tf);

		}
		return view;
	}
}
