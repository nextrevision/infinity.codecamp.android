package com.isd.twitter;

import java.util.ArrayList;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class Main extends Activity {
	private String mQuery = "#android";
	private int mPage = 1;
	private EditText mSearchString;
	private ResourceHelper mResourceHelper;
	private ListView mListView;
	private CustomAdapter mAdapter;
	private Activity mActivity;
	private ProgressDialog mDialog;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mActivity = this;
        
        Button search = (Button) findViewById(R.id.searchButton);
        Button back = (Button) findViewById(R.id.backButton);
        Button next = (Button) findViewById(R.id.nextButton);
        mSearchString = (EditText) findViewById(R.id.searchString);
                
        mDialog = new ProgressDialog(mActivity);
        mDialog.setIndeterminate(true);
		mDialog.setCancelable(false);
		mDialog.setTitle("Please wait:");
		mDialog.setMessage("Getting new tweets...");
        
        mResourceHelper = new ResourceHelper();
        
        ArrayList<TweetModel> tweets = mResourceHelper.getTweets(mQuery, Integer.toString(mPage));

        mListView = (ListView)findViewById(android.R.id.list);
        
        mAdapter = new CustomAdapter(this, R.layout.list_item, tweets);
        
        mListView.setAdapter(mAdapter);
        
        search.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				mQuery = mSearchString.getText().toString();
				if (mQuery.equals("")) {
					Toast.makeText(mActivity, "You must enter in a search term!", Toast.LENGTH_LONG).show();
				} else {
					mDialog.show();
					mAdapter.notifyDataSetInvalidated();
					mListView.clearChoices();
					AsyncHelper asyncHelper = new AsyncHelper(mActivity.getLocalClassName());
					asyncHelper.setMain(Main.this);
					asyncHelper.execute(mQuery, Integer.toString(mPage));
				}	
			}
        });
        
        back.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if (mPage == 1) {
					Toast.makeText(mActivity, "You're on the first page!", Toast.LENGTH_LONG).show();
				} else {
					mDialog.show();
					mPage--;
					mAdapter.notifyDataSetInvalidated();
					mListView.clearChoices();
					AsyncHelper asyncHelper = new AsyncHelper(mActivity.getLocalClassName());
					asyncHelper.setMain(Main.this);
					asyncHelper.execute(mQuery, Integer.toString(mPage));
				}
			}	
        });
        
        next.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				mDialog.show();
				mPage++;
				mAdapter.notifyDataSetInvalidated();
				mListView.clearChoices();
				AsyncHelper asyncHelper = new AsyncHelper(mActivity.getLocalClassName());
				asyncHelper.setMain(Main.this);
				asyncHelper.execute(mQuery, Integer.toString(mPage));
			}

        });
        
        
        mListView.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				//Toast.makeText(activity, "ID: " + adapter.getItem(position).idStr, Toast.LENGTH_LONG).show();
				Intent in = new Intent(mActivity, Tweet.class);
				in.putExtra("id", mAdapter.getItem(position).idStr);
				startActivity(in);				
			}
        });
    } // End of onCreate
    
    public void refreshView(DTO dto) {
    	mAdapter = new CustomAdapter(mActivity, R.layout.list_item, dto.tweets);
		mListView.setAdapter(mAdapter);
		mAdapter.notifyDataSetChanged();
		mDialog.hide();
    }
    

    ////////////////////////////////////////////////////////////////////////////////////
   // 	You can reference AsyncTasks a few different ways, either by including them
   //		in your class as a subclass or by calling a separate class and initiating
   //		a call back.
    ////////////////////////////////////////////////////////////////////////////////////
    
//    private class SearchTask extends AsyncTask<String, Void, ArrayList<ResultsModel>> {
//
//		@Override
//		protected ArrayList<ResultsModel> doInBackground(String... params) {
//			ArrayList<ResultsModel> tweets = mResourceHelper.getTweets(params[0]);
//			return tweets;
//		}
//		
//		protected void onPreExecute() {
//			mAdapter.clear();
//		}
//		
//		protected void onPostExecute(ArrayList<ResultsModel> tweets) {
//			mAdapter = new CustomAdapter(mActivity, R.layout.list_item, tweets);
//			mListView.setAdapter(mAdapter);
//			mAdapter.notifyDataSetChanged();
//		}
//    }
   
}