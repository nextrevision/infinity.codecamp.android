package com.isd.twitter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class ResourceHelper {

	private static String sUrl;

	public ResourceHelper() {
	}

	public ArrayList<TweetModel> getTweets(String q, String p) {

		sUrl = "http://search.twitter.com/search.json?q=" + q + "&rpp=10&page="
				+ p;

		Gson gson = new Gson();

		InputStream is = retrieveContent(sUrl);

		Reader reader = new InputStreamReader(is);

		SearchModel searchResults = gson.fromJson(reader, SearchModel.class);

		ArrayList<TweetModel> results = searchResults.results;

		return results;

		// for (ResultsModel result : results) {
		// // Could insert into DB, return the list to the user,
		// // or if this weren't an AsyncTask, display the results
		// // via Toast
		// }
	}

	public TweetModel getTweet(String id) {

		sUrl = "http://api.twitter.com/1/statuses/show.json?id=" + id
				+ "&include_entities=false";

		Gson gson = new Gson();

		InputStream is = retrieveContent(sUrl);

		Reader reader = new InputStreamReader(is);

		TweetModel tweetResults = gson.fromJson(reader, TweetModel.class);

		return tweetResults;

	}

	public UserModel getUser(String id) {

		sUrl = "http://api.twitter.com/1/users/show.json?user_id=" + id;

		Gson gson = new Gson();

		InputStream is = retrieveContent(sUrl);

		Reader reader = new InputStreamReader(is);

		UserModel userResults = gson.fromJson(reader, UserModel.class);

		return userResults;

	}

	public ArrayList<TweetModel> getUserTweets(String id) {

		sUrl = "http://api.twitter.com/1/statuses/user_timeline.json?count=10&user_id="
				+ id;

		Gson gson = new Gson();

		InputStream is = retrieveContent(sUrl);

		String json = null;

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			json = sb.toString();
		} catch (Exception e) {
			// YOU SHOULD NEVER DO THIS!!!
		}
		JsonElement jsonElement = new JsonParser().parse(json);
		JsonArray array = jsonElement.getAsJsonArray();

		Iterator iterator = array.iterator();
		ArrayList<TweetModel> resultsModel = new ArrayList<TweetModel>();

		while (iterator.hasNext()) {
			JsonElement jsonRecord = (JsonElement) iterator.next();
			TweetModel record = gson.fromJson(jsonRecord, TweetModel.class);
			resultsModel.add(record);
		}

		return resultsModel;

	}

	private InputStream retrieveContent(String url) {

		DefaultHttpClient client = new DefaultHttpClient();

		HttpGet request = new HttpGet(url);

		try {
			HttpResponse response = client.execute(request);
			int statusCode = response.getStatusLine().getStatusCode();

			if (statusCode != 200) {
				Log.e("The status code returned was " + statusCode, null);
				return null;
			}

			HttpEntity responseEntity = response.getEntity();

			InputStream is = responseEntity.getContent();

			return is;

		} catch (Exception e) { // it is bad practice to catch general
								// exceptions,
								// for demo purposes, it will due.
								// Log this exception
			Log.e("Great Scott!", e.getCause().toString());
		}
		return null;
	}

	public Bitmap getBitmap(String imgUrl) {
		Bitmap bitmap = null;
		InputStream is = null;
		try {
			URL url = new URL(imgUrl);
			URLConnection urlConn = url.openConnection();
			HttpURLConnection httpConn = (HttpURLConnection) urlConn;
			httpConn.connect();
			is = httpConn.getInputStream();
			bitmap = BitmapFactory.decodeStream(is);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				Log.e("Error: ", e.getLocalizedMessage());
			}
		}
		return bitmap;
	}

}
