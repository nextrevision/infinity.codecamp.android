package com.isd.twitter;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class UserCustomAdapter extends ArrayAdapter<TweetModel> {
	private ArrayList<TweetModel> tweets;
	private Activity activity;
	private TextView userSingleTweet;
	private Typeface tf;

	public UserCustomAdapter(Activity activity, int viewId,
			ArrayList<TweetModel> tweets) {
		super(activity, R.layout.user_list_item, tweets);
		this.activity = activity;
		this.tweets = tweets;
		tf = Typeface.createFromAsset(activity.getAssets(),
				"fonts/Mako-Regular.ttf");
	}

	public View getView(int position, View v, ViewGroup parent) {
		View view = v;
		TweetModel tweet = tweets.get(position);
		if (v == null) {
			LayoutInflater inflator = activity.getLayoutInflater();
			view = inflator.inflate(R.layout.user_list_item, null);
			userSingleTweet = (TextView) view
					.findViewById(R.id.userSingleTweet);
		}
		if (tweet != null) {
			userSingleTweet.setText(tweet.text);
			userSingleTweet.setTypeface(tf);
		}
		return view;
	}
}
