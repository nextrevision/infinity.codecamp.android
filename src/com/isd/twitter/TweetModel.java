package com.isd.twitter;

import com.google.gson.annotations.SerializedName;

public class TweetModel {

	UserModel user;
	
	@SerializedName("from_user")
	public String fromUser;
	
	@SerializedName("created_at")
	public String createdDate;
	
	@SerializedName("id_str")
	public String idStr;
	
	public String text;
	
	@SerializedName("retweet_count")
	public int rtCount;
	
}
