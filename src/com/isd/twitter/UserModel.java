package com.isd.twitter;

import com.google.gson.annotations.SerializedName;

public class UserModel {
		
	public String name;
	
	@SerializedName("profile_image_url")
	public String imgUrl;
	
	public String location;
	
	@SerializedName("favourites_count")
	public int favorites;
	
	public String url;
	
	@SerializedName("followers_count")
	public int followers;
	
	@SerializedName("protected")
	public boolean profileProtected;
	
	public String description;
	
	@SerializedName("statuses_count")
	public long statuses;
	
	@SerializedName("friends_count")
	public long friends;
	
	@SerializedName("screen_name")
	public String handle;
	
	@SerializedName("id_str")
	public String idStr;
	
}
