package com.isd.twitter;

import android.os.AsyncTask;

class AsyncHelper extends AsyncTask<String, Void, DTO> {
	
	Main main;
	Tweet tweet;
	User user;
	private int mClassNumber = 0;
	
	public AsyncHelper(String className) {
		if (className.equals("Main") || className == "Main") {
			mClassNumber = 1;
		} else if (className.equals("Tweet")) {
			mClassNumber = 2;
		} else if (className.equals("User")) {
			mClassNumber = 3;
		}
	}
	
	public void setTweet(Tweet tweet) {
		this.tweet = tweet;
	}
	
	public void setMain(Main main) {
		this.main = main;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	@Override
	protected DTO doInBackground(String... params) {
		DTO dto = new DTO();
		ResourceHelper resourceHelper = new ResourceHelper();
		
		switch (mClassNumber) {
		case 1:
			dto.setSearchModel(resourceHelper.getTweets(params[0], params[1]));
			break;
		case 2:
			dto.setTweetModel(resourceHelper.getTweet(params[0]));
			dto.setBitmap(resourceHelper.getBitmap(dto.tweetModel.user.imgUrl));
			break;
		case 3:
			dto.setUserModel(resourceHelper.getUser(params[0]));
			dto.setSearchModel(resourceHelper.getUserTweets(params[0]));
			dto.setBitmap(resourceHelper.getBitmap(dto.user.imgUrl));
			break;
		default:
			// Don't do anything
			break;
		}
		
		return dto;
	}
	
	protected void onPostExecute(DTO dto) {
		switch (mClassNumber) {
		case 1:
			this.main.refreshView(dto);
			break;
		case 2:
			this.tweet.onResourceComplete(dto);
			break;
		case 3:
			this.user.onComplete(dto);
			break;
		default:
			// Do nothing
			break;
		}
		
	}
}