package com.isd.twitter;

import java.util.ArrayList;

import android.graphics.Bitmap;

public class DTO {

	public Bitmap bitmap;
	public TweetModel tweetModel;
	public ArrayList<TweetModel> tweets;
	public UserModel user;
	
	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}
	
	public void setTweetModel(TweetModel tweetModel) {
		this.tweetModel = tweetModel;
	}

	public void setSearchModel(ArrayList<TweetModel> tweets) {
		this.tweets = tweets;	
	}
	
	public ArrayList<TweetModel> getSearchModel() {
		return tweets;
	}

	public void setUserModel(UserModel user) {
		this.user = user;
	}
	
}
